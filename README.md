# Airods Docker-in-Docker

Docker-in-Docker solution for launching the Airods HTTP API.


## Prerequisites

To run the API, access to a Mongo database and a running iRODS instance are required.


## Setup and launch the API

To setup and launch the HTTP API, follow these steps:

1. Rename the config file `projectrc` to `.projectrc`, and edit it with the necessary connection informations.

2. Run the `setup.sh` installation script. At the end of a successful setup, the command prompt should be inside the Docker container, and the following message should be displayed:
```
Launch the REST HTTP-API server with the command:
#restapi launch

Run unittests with:
#restapi tests
```

3. Run the API, with the command `restapi launch`.


## Check the status of the API

### Web

If the server is up, the page <http://localhost:8080/api/status> should show a message saying "Server is alive!"

### Swagger interface

In a different terminal windows, a Swagger interface can be launched with the command:

```docker exec -it dind-airods rapydo interfaces swagger```

That will be available on port 8080.
